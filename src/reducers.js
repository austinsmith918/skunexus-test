import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import history from 'utils/history';
import userListReducer from 'containers/UserList/reducer';
import userDetailReducer from 'containers/UserDetail/reducer';

export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    userList: userListReducer,
    userDetail: userDetailReducer,
    router: connectRouter(history),
    ...injectedReducers,
  });

  return rootReducer;
}
