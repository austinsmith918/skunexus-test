import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { Card, CardBody, CardTitle, CardText, Button } from 'reactstrap';

const UserListCard = ({ user }) => {
  const history = useHistory();

  return (
    <Card>
      <CardBody>
        <CardTitle className="font-weight-bold">
          {user.first_name} {user.last_name}
        </CardTitle>
        <CardText>{user.email}</CardText>
        <CardText>{user.cc_number}</CardText>
        <CardText>{user.cc_type}</CardText>
        <Button
          outline
          block
          color="info"
          className="mt-2 text-uppercase"
          onClick={() => history.push(`/user/${user.id}`)}
        >
          Details
        </Button>
      </CardBody>
    </Card>
  );
};

UserListCard.propTypes = {
  user: PropTypes.object.isRequired,
};

export default UserListCard;
