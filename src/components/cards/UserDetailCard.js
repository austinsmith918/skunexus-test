import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardText, Row, Col } from 'reactstrap';

const UserDetailCard = ({ user, totalCost }) => (
  <Card body>
    <Row>
      <Col md="9">
        <CardText>{user.email}</CardText>
        <CardText>{user.cc_number}</CardText>
        <CardText>{user.cc_type}</CardText>
      </Col>
      <Col md="3" className="text-center">
        <h1>{totalCost.toFixed(2)}</h1>
        <CardText>{user.currency}</CardText>
      </Col>
    </Row>
  </Card>
);

UserDetailCard.propTypes = {
  user: PropTypes.object.isRequired,
  totalCost: PropTypes.number,
};

UserDetailCard.defaultProps = {
  totalCost: 0,
};

export default UserDetailCard;
