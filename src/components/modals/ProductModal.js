import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import {
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  Row,
  Col,
  Label,
  Button,
} from 'reactstrap';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { ACTIONS } from 'utils/constants';

const ProductModal = ({ mode, isOpen, saving, product, toggle, onSubmit }) => {
  const formRef = useRef();

  const onFormSubmit = ({ name, cost }) => {
    onSubmit({ name, cost });
  };

  const formInitialValues = () => {
    let initialValues = {
      name: '',
      cost: '',
    };

    if (mode === ACTIONS.EDIT && product) {
      initialValues = {
        name: product.name,
        cost: product.cost,
      };
    }

    return initialValues;
  };

  return (
    <Modal isOpen={isOpen} toggle={toggle} size="sm" backdrop="static">
      <ModalHeader toggle={toggle}>
        {mode === ACTIONS.CREATE ? 'Add' : 'Edit'} Product
      </ModalHeader>
      <ModalBody>
        <Formik
          innerRef={formRef}
          enableReinitialize
          initialValues={formInitialValues()}
          validationSchema={Yup.object().shape({
            name: Yup.string().required('Please enther the name'),
            cost: Yup.number()
              .required('Please input the cost')
              .moreThan(0, 'Cost must be greater than 0'),
          })}
          onSubmit={onFormSubmit}
        >
          <Form className="av-tooltip tooltip-label-bottom">
            <Row>
              <Col xs="12" className="mb-2">
                <FormGroup className="form-group has-float-label">
                  <Label>Name</Label>
                  <Field className="form-control" type="text" name="name" />
                  <ErrorMessage
                    name="name"
                    className="invalid-feedback d-block"
                    component="div"
                  />
                </FormGroup>
              </Col>
              <Col xs="12" className="mb-2">
                <FormGroup className="form-group has-float-label">
                  <Label>Cost</Label>
                  <Field className="form-control" type="number" name="cost" />
                  <ErrorMessage
                    name="cost"
                    className="invalid-feedback d-block"
                    component="div"
                  />
                </FormGroup>
              </Col>
              <Col xs="12">
                <Button type="submit" color="primary" block disabled={saving}>
                  Save
                </Button>
              </Col>
            </Row>
          </Form>
        </Formik>
      </ModalBody>
    </Modal>
  );
};

ProductModal.propTypes = {
  mode: PropTypes.string.isRequired,
  isOpen: PropTypes.bool,
  saving: PropTypes.bool,
  product: PropTypes.object,
  toggle: PropTypes.func,
  onSubmit: PropTypes.func,
};

ProductModal.defaultProps = {
  isOpen: false,
  saving: false,
  product: null,
  toggle: () => {},
  onSubmit: () => {},
};

export default ProductModal;
