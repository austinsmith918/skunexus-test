import React from 'react';
import PropTypes from 'prop-types';
import { Table, Button } from 'reactstrap';

const ProductsTable = ({
  products,
  onAddProduct,
  onEditProduct,
  onDeleteProduct,
}) => (
  <>
    <div className="d-flex my-4">
      <h3 className="mr-auto">Products</h3>
      <Button
        color="primary"
        className="px-4 text-uppercase"
        onClick={() => onAddProduct()}
      >
        Add
      </Button>
    </div>
    <Table responsive>
      <tbody>
        {products.map(({ id, name, cost }) => (
          <tr key={id}>
            <td>{name}</td>
            <td>{cost.toFixed(2)}</td>
            <td className="d-flex flex-grow-1 justify-content-end">
              <Button
                outline
                color="info"
                className="px-4"
                onClick={() => onEditProduct({ id, name, cost })}
              >
                Edit
              </Button>
              <Button
                outline
                color="danger"
                className="px-4 ml-2"
                onClick={() => onDeleteProduct(id)}
              >
                Delete
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  </>
);

ProductsTable.propTypes = {
  products: PropTypes.array,
  onAddProduct: PropTypes.func,
  onEditProduct: PropTypes.func,
  onDeleteProduct: PropTypes.func,
};

ProductsTable.defaultProps = {
  products: [],
  onAddProduct: () => {},
  onEditProduct: () => {},
  onDeleteProduct: () => {},
};

export default ProductsTable;
