import React from 'react';
import PropTypes from 'prop-types';
import { Spinner } from 'reactstrap';

const LoadingSpinner = ({ type, color, size }) => (
  <div className="loading">
    <Spinner type={type} color={color} size={size}></Spinner>
  </div>
);

LoadingSpinner.propTypes = {
  type: PropTypes.string,
  color: PropTypes.string,
  size: PropTypes.string,
};

LoadingSpinner.defaultProps = {
  type: 'border',
  color: 'primary',
  size: 'md',
};

export default LoadingSpinner;
