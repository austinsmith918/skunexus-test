import React from 'react';
import { Container, Row, Col, Card, CardTitle, Button } from 'reactstrap';
import { ERROR_MESSAGES } from 'utils/constants';

const NotFound = () => (
  <Container>
    <Row>
      <Col md="10" lg="6" className="mx-auto mt-5">
        <Card body className="text-center">
          <CardTitle className="mb-4">
            {ERROR_MESSAGES.NOT_FOUND_PAGE_TITILE}
          </CardTitle>
          <p className="mb-0 text-muted text-small mb-0">
            {ERROR_MESSAGES.NOT_FOUND_PAGE_CONTENT}
          </p>
          <p className="display-1 font-weight-bold mb-5">404</p>
          <Button href="/" color="primary" size="lg">
            GO BACK HOME
          </Button>
        </Card>
      </Col>
    </Row>
  </Container>
);

export default NotFound;
