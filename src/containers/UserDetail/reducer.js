import {
  FETCH_USER,
  FETCH_USER_SUCCESS,
  FETCH_USER_ERROR,
  CREATE_USER_PRODUCT,
  CREATE_USER_PRODUCT_SUCCESS,
  CREATE_USER_PRODUCT_ERROR,
  UPDATE_USER_PRODUCT,
  UPDATE_USER_PRODUCT_SUCCESS,
  UPDATE_USER_PRODUCT_ERROR,
  DELETE_USER_PRODUCT,
  DELETE_USER_PRODUCT_SUCCESS,
  DELETE_USER_PRODUCT_ERROR,
} from './constants';

export const initialState = {
  user: {
    loading: undefined,
    error: undefined,
    data: undefined,
    products: undefined,
  },
};

const userReducer = (state = initialState, action) => {
  const { error, data, products, product, deletedId } = action.payload || {};
  let newProducts = null;

  switch (action.type) {
    case FETCH_USER:
      return {
        user: {
          loading: true,
          error: false,
          data: false,
          products: [],
        },
      };
    case FETCH_USER_SUCCESS:
      return {
        user: {
          loading: false,
          error: null,
          data,
          products,
        },
      };
    case FETCH_USER_ERROR:
      return {
        user: {
          loading: false,
          error,
          data: false,
          products: [],
        },
      };
    case CREATE_USER_PRODUCT:
    case UPDATE_USER_PRODUCT:
    case DELETE_USER_PRODUCT:
      return {
        user: {
          ...state.user,
          loading: true,
          error: false,
        },
      };
    case CREATE_USER_PRODUCT_SUCCESS:
      newProducts = [...state.user.products, product];
      return {
        user: {
          ...state.user,
          loading: false,
          error: null,
          products: newProducts,
        },
      };
    case UPDATE_USER_PRODUCT_SUCCESS:
      newProducts = state.user.products.map((item) =>
        item.id === product.id ? product : item,
      );
      return {
        user: {
          ...state.user,
          loading: false,
          error: null,
          products: newProducts,
        },
      };
    case DELETE_USER_PRODUCT_SUCCESS:
      newProducts = state.user.products.filter((item) => item.id !== deletedId);
      return {
        user: {
          ...state.user,
          loading: false,
          error: null,
          products: newProducts,
        },
      };
    case CREATE_USER_PRODUCT_ERROR:
    case UPDATE_USER_PRODUCT_ERROR:
    case DELETE_USER_PRODUCT_ERROR:
      return {
        user: {
          ...state.user,
          loading: false,
          error,
        },
      };
    default:
      return state;
  }
};

export default userReducer;
