import {
  FETCH_USER,
  FETCH_USER_SUCCESS,
  FETCH_USER_ERROR,
  CREATE_USER_PRODUCT,
  CREATE_USER_PRODUCT_SUCCESS,
  CREATE_USER_PRODUCT_ERROR,
  UPDATE_USER_PRODUCT,
  UPDATE_USER_PRODUCT_SUCCESS,
  UPDATE_USER_PRODUCT_ERROR,
  DELETE_USER_PRODUCT,
  DELETE_USER_PRODUCT_SUCCESS,
  DELETE_USER_PRODUCT_ERROR,
} from './constants';

/**
 * Fetch user, this action starts the request saga
 *
 * @return {object} An action object with a type of FETCH_USER
 */
export function fetchUser({ id }) {
  return {
    type: FETCH_USER,
    payload: { id },
  };
}

/**
 * Dispatched when the user is fetched by the request saga
 *
 * @param  {object} user user info
 *
 * @return {object} An action object with a type of FETCH_USER_SUCCESS passing the user
 */
export function fetchUserSucceeded({ data: { data, products } }) {
  return {
    type: FETCH_USER_SUCCESS,
    payload: { data, products },
  };
}

/**
 * Dispatched when fetching user fails
 *
 * @param  {object} error The error
 *
 * @return {object} An action object with a type of FETCH_USER_ERROR passing the error
 */
export function fetchUserFailed(error) {
  return {
    type: FETCH_USER_ERROR,
    payload: { error },
  };
}

/**
 * Create user product, this action starts the request saga
 *
 * @return {object} An action object with a type of CREATE_USER_PRODUCT
 */
export function createUserProduct({ userId, name, cost }) {
  return {
    type: CREATE_USER_PRODUCT,
    payload: { userId, name, cost },
  };
}

/**
 * Dispatched when the product is created by the request saga
 *
 * @param  {object} product created product info
 *
 * @return {object} An action object with a type of CREATE_USER_PRODUCT_SUCCESS passing the product
 */
export function createUserProductSucceeded({ data: { product } }) {
  return {
    type: CREATE_USER_PRODUCT_SUCCESS,
    payload: { product },
  };
}

/**
 * Dispatched when creating user product fails
 *
 * @param  {object} error The error
 *
 * @return {object} An action object with a type of CREATE_USER_PRODUCT_ERROR passing the error
 */
export function createUserProductFailed(error) {
  return {
    type: CREATE_USER_PRODUCT_ERROR,
    payload: { error },
  };
}

/**
 * Update user product, this action starts the request saga
 *
 * @return {object} An action object with a type of UPDATE_USER_PRODUCT
 */
export function updateUserProduct({ userId, id, name, cost }) {
  return {
    type: UPDATE_USER_PRODUCT,
    payload: {
      userId,
      id,
      name,
      cost,
    },
  };
}

/**
 * Dispatched when the product is updated by the request saga
 *
 * @param  {object} product updated product info
 *
 * @return {object} An action object with a type of UPDATE_USER_PRODUCT_SUCCESS passing the product
 */
export function updateUserProductSucceeded({ data: { product } }) {
  return {
    type: UPDATE_USER_PRODUCT_SUCCESS,
    payload: { product },
  };
}

/**
 * Dispatched when updating product fails
 *
 * @param  {object} error The error
 *
 * @return {object} An action object with a type of UPDATE_USER_PRODUCT_ERROR passing the error
 */
export function updateUserProductFailed(error) {
  return {
    type: UPDATE_USER_PRODUCT_ERROR,
    payload: { error },
  };
}

/**
 * Delete user product, this action starts the request saga
 *
 * @return {object} An action object with a type of DELETE_USER_PRODUCT
 */
export function deleteUserProduct({ userId, id }) {
  return {
    type: DELETE_USER_PRODUCT,
    payload: {
      userId,
      id,
    },
  };
}

/**
 * Dispatched when the product is deleted by the request saga
 *
 * @param  {object} deletedId deleted product id
 *
 * @return {object} An action object with a type of DELETE_USER_PRODUCT_SUCCESS passing the product id
 */
export function deleteUserProductSucceeded({ data: { deletedId } }) {
  return {
    type: DELETE_USER_PRODUCT_SUCCESS,
    payload: { deletedId },
  };
}

/**
 * Dispatched when deleting product fails
 *
 * @param  {object} error The error
 *
 * @return {object} An action object with a type of DELETE_USER_PRODUCT_ERROR passing the error
 */
export function deleteUserProductFailed(error) {
  return {
    type: DELETE_USER_PRODUCT_ERROR,
    payload: { error },
  };
}
