import { call, fork, takeLatest } from 'redux-saga/effects';
import { appApiSaga } from 'containers/App/saga';
import { makeJsonRequestOptions } from 'utils/request';
import {
  FETCH_USER,
  CREATE_USER_PRODUCT,
  UPDATE_USER_PRODUCT,
  DELETE_USER_PRODUCT,
} from './constants';
import {
  fetchUserSucceeded,
  fetchUserFailed,
  createUserProductSucceeded,
  createUserProductFailed,
  updateUserProductSucceeded,
  updateUserProductFailed,
  deleteUserProductSucceeded,
  deleteUserProductFailed,
} from './actions';

/**
 * FETCH_USER saga
 */
export function* fetchUser(action) {
  const { id } = action.payload;

  const options = makeJsonRequestOptions({
    method: 'GET',
    requestUrlPath: `users/${id}`,
  });

  yield call(appApiSaga, options, [fetchUserSucceeded], fetchUserFailed);
}

export function* fetchUserWatcher() {
  yield takeLatest(FETCH_USER, fetchUser);
}

/**
 * CREATE_USER_PRODUCT saga
 */
export function* createUserProduct(action) {
  const { userId, name, cost } = action.payload;

  const options = makeJsonRequestOptions({
    method: 'POST',
    requestUrlPath: `users/${userId}/products`,
    data: { name, cost },
  });

  yield call(
    appApiSaga,
    options,
    [createUserProductSucceeded],
    createUserProductFailed,
  );
}

export function* createUserProductWatcher() {
  yield takeLatest(CREATE_USER_PRODUCT, createUserProduct);
}

/**
 * UPDATE_USER_PRODUCT saga
 */
export function* updateUserProduct(action) {
  const { userId, id, name, cost } = action.payload;

  const options = makeJsonRequestOptions({
    method: 'PUT',
    requestUrlPath: `users/${userId}/products`,
    data: { id, name, cost },
  });

  yield call(
    appApiSaga,
    options,
    [updateUserProductSucceeded],
    updateUserProductFailed,
  );
}

export function* updateUserProductWatcher() {
  yield takeLatest(UPDATE_USER_PRODUCT, updateUserProduct);
}

/**
 * DELETE_USER_PRODUCT saga
 */
export function* deleteUserProduct(action) {
  const { userId, id } = action.payload;

  const options = makeJsonRequestOptions({
    method: 'DELETE',
    requestUrlPath: `users/${userId}/products`,
    data: { id },
  });

  yield call(
    appApiSaga,
    options,
    [deleteUserProductSucceeded],
    deleteUserProductFailed,
  );
}

export function* deleteUserProductWatcher() {
  yield takeLatest(DELETE_USER_PRODUCT, deleteUserProduct);
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* userMainSaga() {
  yield fork(fetchUserWatcher);
  yield fork(createUserProductWatcher);
  yield fork(updateUserProductWatcher);
  yield fork(deleteUserProductWatcher);
}
