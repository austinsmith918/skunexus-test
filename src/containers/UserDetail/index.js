import React, { useState, useCallback, useEffect } from 'react';
import { compose } from 'redux';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, NavLink } from 'react-router-dom';
import { sumBy } from 'lodash';
import { Navbar, Nav, NavItem, Container } from 'reactstrap';
import confirm from 'reactstrap-confirm';
import { LoadingSpinner } from 'components/spinners';
import { UserDetailCard } from 'components/cards';
import { ProductsTable } from 'components/tables';
import { ProductModal } from 'components/modals';
import injectSaga from 'utils/injectSaga';
import { ACTIONS } from 'utils/constants';
import {
  fetchUser,
  createUserProduct,
  updateUserProduct,
  deleteUserProduct,
} from './actions';
import saga from './saga';
import { selectUser } from './selectors';

const UserDetail = () => {
  const { userId } = useParams();

  const [modal, setModal] = useState({
    isOpen: false,
    action: ACTIONS.NONE,
  });

  const { loading, data: userData, products: userProducts = [] } = useSelector(
    selectUser,
  );

  const dispatch = useDispatch();
  const callUserDetail = useCallback(({ id }) => dispatch(fetchUser({ id })), [
    dispatch,
  ]);
  const callCreteUserProduct = useCallback(
    ({ uId, name, cost }) =>
      dispatch(createUserProduct({ userId: uId, name, cost })),
    [dispatch],
  );
  const callUpdateUserProduct = useCallback(
    ({ uId, pId, name, cost }) =>
      dispatch(updateUserProduct({ userId: uId, id: pId, name, cost })),
    [dispatch],
  );
  const callDeleteUserProduct = useCallback(
    ({ uId, pId }) => dispatch(deleteUserProduct({ userId: uId, id: pId })),
    [dispatch],
  );

  useEffect(() => {
    const fetchUserDeail = () => callUserDetail({ id: userId });
    fetchUserDeail();
  }, [callUserDetail, userId]);

  useEffect(() => {
    if (
      !loading &&
      modal.isOpen &&
      (modal.mode === ACTIONS.CREATE || modal.mode === ACTIONS.EDIT)
    ) {
      setModal({
        isOpen: false,
        mode: ACTIONS.NONE,
        product: null,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  const onSaveProduct = ({ name, cost }) => {
    const { mode, product } = modal;

    if (mode === ACTIONS.CREATE) {
      callCreteUserProduct({ uId: userId, name, cost });
    } else if (mode === ACTIONS.EDIT) {
      callUpdateUserProduct({ uId: userId, pId: product.id, name, cost });
    }
  };

  const onDeleteProduct = async (productId) => {
    const confirmResult = await confirm({
      title: <span className="text-danger">Delete Confirmation</span>,
      message: 'Do you want to remove this product from the list?',
      confirmText: 'Confirm',
    });
    if (confirmResult) {
      callDeleteUserProduct({ uId: userId, pId: productId });
    }
  };

  if (!userData) {
    return <LoadingSpinner color="secondary" />;
  }

  return (
    <>
      <Navbar color="light" light expand>
        <Nav navbar>
          <NavItem>
            <NavLink to="/">
              <i className="iconsminds-left"></i>
            </NavLink>
          </NavItem>
          <NavItem className="ml-4">
            <span>
              {userData.first_name} {userData.last_name}
            </span>
          </NavItem>
        </Nav>
      </Navbar>
      <Container className="my-4">
        <UserDetailCard
          user={userData}
          totalCost={sumBy(userProducts, (product) => product.cost)}
        ></UserDetailCard>
        <ProductsTable
          products={userProducts}
          onAddProduct={() => {
            setModal({
              isOpen: true,
              mode: ACTIONS.CREATE,
              product: null,
            });
          }}
          onEditProduct={(product) => {
            setModal({
              isOpen: true,
              mode: ACTIONS.EDIT,
              product,
            });
          }}
          onDeleteProduct={(productId) => onDeleteProduct(productId)}
        ></ProductsTable>
        {modal.isOpen && (
          <ProductModal
            mode={modal.mode}
            isOpen={modal.isOpen}
            product={modal.product}
            toggle={() => {
              setModal({
                isOpen: false,
                mode: ACTIONS.NONE,
                product: null,
              });
            }}
            onSubmit={({ name, cost }) => {
              onSaveProduct({ name, cost });
            }}
            saving={
              modal.isOpen &&
              (modal.mode === ACTIONS.CREATE || modal.mode === ACTIONS.EDIT) &&
              loading
            }
          ></ProductModal>
        )}
      </Container>
    </>
  );
};

const withSaga = injectSaga({ key: 'userDetail', saga });
export default compose(withSaga)(UserDetail);
