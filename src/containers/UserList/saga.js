import { call, fork, takeLatest } from 'redux-saga/effects';
import { appApiSaga } from 'containers/App/saga';
import { makeJsonRequestOptions } from 'utils/request';
import { FETCH_USERS } from './constants';
import { fetchUsersSucceeded, fetchUsersFailed } from './actions';

/**
 * FETCH_USERS saga
 */
export function* fetchUsers(action) {
  const { search } = action.payload;
  const params = {};

  if (search !== '') {
    params.search = search;
  }

  const options = makeJsonRequestOptions({
    method: 'GET',
    requestUrlPath: `users`,
    params,
  });

  yield call(appApiSaga, options, [fetchUsersSucceeded], fetchUsersFailed);
}

export function* fetchUsersWatcher() {
  yield takeLatest(FETCH_USERS, fetchUsers);
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* userListMainSaga() {
  yield fork(fetchUsersWatcher);
}
