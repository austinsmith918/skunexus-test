import React, { useState, useCallback, useEffect } from 'react';
import { compose } from 'redux';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Row, Col } from 'reactstrap';
import injectSaga from 'utils/injectSaga';
import { UserListCard } from 'components/cards';
import { fetchUsers } from './actions';
import saga from './saga';
import { currentUsers } from './selectors';

const UserList = () => {
  const [query, setQuery] = useState('');

  const users = useSelector(currentUsers);

  const dispatch = useDispatch();
  const callUserList = useCallback(
    ({ search }) => dispatch(fetchUsers({ search })),
    [dispatch],
  );

  useEffect(() => {
    const fetchUserList = () => callUserList({ search: query });
    fetchUserList();
  }, [callUserList, query]);

  const onSearchUsers = (e) => {
    e.persist();

    if (e.key === 'Enter') {
      setQuery(e.target.value);
    }
  };

  return (
    <Container className="my-4">
      <Row>
        <Col md="6" xl="4" className="ml-auto my-2 p-2">
          <input
            type="text"
            name="keyword"
            id="search"
            className="form-control"
            placeholder="Search by cc number"
            defaultValue={query}
            onKeyPress={(e) => onSearchUsers(e)}
          />
        </Col>
      </Row>
      <Row>
        {users.data.map((user) => (
          <Col sm="6" md="4" xl="3" className="p-2" key={user.id}>
            <UserListCard user={user}></UserListCard>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

const withSaga = injectSaga({ key: 'userList', saga });
export default compose(withSaga)(UserList);
