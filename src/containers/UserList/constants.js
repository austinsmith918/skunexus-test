export const FETCH_USERS = 'UserList/FETCH_USERS';
export const FETCH_USERS_SUCCESS = 'UserList/FETCH_USERS_SUCCESS';
export const FETCH_USERS_ERROR = 'UserList/FETCH_USERS_ERROR';
