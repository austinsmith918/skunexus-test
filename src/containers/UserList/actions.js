import {
  FETCH_USERS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_ERROR,
} from './constants';

/**
 * Fetch users, this action starts the request saga
 *
 * @return {object} An action object with a type of FETCH_USERS
 */
export function fetchUsers({ search }) {
  return {
    type: FETCH_USERS,
    payload: { search },
  };
}

/**
 * Dispatched when the users are fetched by the request saga
 *
 * @param  {object} users users info
 *
 * @return {object} An action object with a type of FETCH_USERS_SUCCESS passing the users
 */
export function fetchUsersSucceeded({ data }) {
  return {
    type: FETCH_USERS_SUCCESS,
    payload: { result: data },
  };
}

/**
 * Dispatched when fetching users fails
 *
 * @param  {object} error The error
 *
 * @return {object} An action object with a type of FETCH_USERS_ERROR passing the error
 */
export function fetchUsersFailed(error) {
  return {
    type: FETCH_USERS_ERROR,
    payload: { error },
  };
}
