import {
  FETCH_USERS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_ERROR,
} from './constants';

export const initialState = {
  users: {
    loading: false,
    error: null,
    data: [],
  },
};

const userListReducer = (state = initialState, action) => {
  const { error, result = {} } = action.payload || {};

  switch (action.type) {
    case FETCH_USERS:
      return {
        ...state,
        users: {
          loading: true,
          error: null,
          data: [],
        },
      };
    case FETCH_USERS_SUCCESS:
      return {
        users: {
          loading: false,
          error: null,
          data: result.users,
        },
      };
    case FETCH_USERS_ERROR:
      return {
        users: {
          loading: false,
          error,
          data: [],
        },
      };
    default:
      return state;
  }
};

export default userListReducer;
