import { put, call } from 'redux-saga/effects';
import request from 'utils/request';

/**
 * Base saga
 */
export function* appApiSaga(options, successHandlers, errorHandler) {
  try {
    const response = yield call(request, options);
    for (let i = 0; i < successHandlers.length; i++) {
      yield put(successHandlers[i](response.data));
    }
  } catch (err) {
    const { response: errResponse } = err;
    yield put(errorHandler(errResponse.data));
  }
}
