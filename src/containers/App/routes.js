import React, { Suspense } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

const UserList = React.lazy(() =>
  import(/* webpackChunkName: "app-userlist" */ 'containers/UserList'),
);
const UserDetail = React.lazy(() =>
  import(/* webpackChunkName: "app-userlist" */ 'containers/UserDetail'),
);
const NotFound = React.lazy(() =>
  import(/* webpackChunkName: "app-notfound" */ 'containers/NotFound'),
);

// eslint-disable-next-line react/prop-types
const ControlledRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => <Component {...props} />} />
);

const routes = () => (
  <Suspense fallback={<div className="loading" />}>
    <Router>
      <Switch>
        <ControlledRoute exact path="/" component={UserList} />
        <ControlledRoute exact path="/user/:userId" component={UserDetail} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  </Suspense>
);

export default routes;
