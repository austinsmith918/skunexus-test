import React from 'react';
import { withRouter } from 'react-router-dom';
import routes from './routes';

const App = () => <div className="h-100">{routes()}</div>;

export default withRouter(App);
