import httpStatus from 'http-status';
import Util from '../utils/Utils';
import { ERROR_MESSAGES } from '../utils/constants';
import usersData from '../data/users.json';

const util = new Util();

/**
 * User Controller
 * @public
 */
class UserController {
  static getAllUsers(req, res) {
    try {
      const { search } = req.query;
      util.setSuccess(httpStatus.OK, {
        users: search
          ? usersData.filter((user) => user.cc_number.includes(search))
          : usersData,
      });
    } catch (error) {
      util.setError(httpStatus.BAD_REQUEST, error.message);
    }
    return util.send(res);
  }

  static getUser(req, res) {
    try {
      const { id } = req.params;
      const selectedUser = usersData.find((user) => user.id === parseInt(id));
      util.setSuccess(httpStatus.OK, {
        data: selectedUser,
        products: [],
      });
    } catch (error) {
      util.setError(httpStatus.BAD_REQUEST, error.message);
    }
    return util.send(res);
  }

  static createProduct(req, res) {
    if (!req.body.name || !req.body.cost) {
      util.setError(httpStatus.BAD_REQUEST, ERROR_MESSAGES.INCOMPLETE_REQUEST);
      return util.send(res);
    }

    try {
      const { name, cost } = req.body;

      // No database integration
      // Generate random product ID and return the product request for now
      util.setSuccess(httpStatus.OK, {
        product: {
          id: new Date().getTime(),
          name,
          cost,
        },
      });
    } catch (error) {
      util.setError(httpStatus.BAD_REQUEST, error.message);
    }
    return util.send(res);
  }

  static updateProduct(req, res) {
    try {
      const { id, name, cost } = req.body;

      // No database integration, return the product request for now
      util.setSuccess(httpStatus.OK, {
        product: {
          id,
          name,
          cost,
        },
      });
    } catch (error) {
      util.setError(httpStatus.BAD_REQUEST, error.message);
    }
    return util.send(res);
  }

  static deleteProduct(req, res) {
    try {
      const { id } = req.body;

      // No database integration, return the id for now
      util.setSuccess(httpStatus.OK, {
        deletedId: id,
      });
    } catch (error) {
      util.setError(httpStatus.BAD_REQUEST, error.message);
    }
    return util.send(res);
  }
}

export default UserController;
