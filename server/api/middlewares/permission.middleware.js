import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import { ERROR_MESSAGES } from '../utils/constants';
import usersData from '../data/users.json';

/**
 * Check if the user is existing user
 * @public
 */
export const checkUser = async (req, res, next) => {
  const { id } = req.params;
  const selectedUser = usersData.find((user) => user.id === parseInt(id));

  if (!selectedUser) {
    return next(
      new APIError(ERROR_MESSAGES.USER_NOT_FOUND, httpStatus.NOT_FOUND, true),
    );
  }

  return next();
};
