export const ERROR_MESSAGES = {
  API_NOT_FOUND: 'API not found.',
  USER_NOT_FOUND: 'User not found.',
  INCOMPLETE_REQUEST: 'Please provide all required fields.',
};
