import { Router } from 'express';
import UserController from '../../controllers/UserController';
import { checkUser } from '../../middlewares/permission.middleware';

const router = Router();

router.route('/').get(UserController.getAllUsers);

router.route('/:id').get(checkUser, UserController.getUser);

router
  .route('/:id/products')
  .post(checkUser, UserController.createProduct)
  .put(checkUser, UserController.updateProduct)
  .delete(checkUser, UserController.deleteProduct);

export default router;
